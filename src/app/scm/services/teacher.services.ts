
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class TeacherService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Teachers`;
    //private controllerApi = '/api/Teachers'

    constructor(private http: HttpClientExt) { }

    getTeacherRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const teacherDetails = res as any[] || [];
                return teacherDetails;
            }));
    }


    getTeacherRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const teacherDetail = res as any || {};
                return teacherDetail;
            }));
    }

    getTeacherRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const teacherDetails = res as any || {};
                return teacherDetails;
            }));
    }


    private handleErrorObservable(error: Response | any) {
        console.error(`TeacherService: ${error}`);
        return observableThrowError(error.message || error);
    }

}