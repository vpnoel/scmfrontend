
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';


@Injectable()
export class StudentService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/Students`;
    //private controllerApi = '/api/Students'

    constructor(private http: HttpClientExt) { }


    getStudentRecordQuery(queryString: string): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": ""
        }
        return this.http.post(`${url}/Lookup`, data).pipe(
            map(res => {
                const studentDetails = res as any[] || [];
                return studentDetails;
            }));
    }

    getStudentRecordQueryByCampusId(queryString: string, campusId: number): Observable<any[]> {
        const url = `${this.controllerApi}`;
        var data = {
            "QueryString": queryString,
            "Type": "",
            "CampusId": campusId
        }
        return this.http.post(`${url}/LookUpAndFindByCampusId`, data).pipe(
            map(res => {
                const studentDetails = res as any[] || [];
                return studentDetails
            }))
    }

    getStudentRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const studentDetail = res as any || {};
                return studentDetail;
            }));
    }

    getStudentRecord(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const studentDetails = res as any || {};
                return studentDetails;
            }));
    }


    private handleErrorObservable(error: Response | any) {
        console.error(`ReferralTransactionService: ${error}`);
        return observableThrowError(error.message || error);
    }

}