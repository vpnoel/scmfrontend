
import {throwError as observableThrowError,  Observable } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralReasonGroup } from "../interfaces/IReferralReasonGroup";


@Injectable()
export class ReferralReasonGroupService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralReasonGroupAssociate`;

    constructor(private http: HttpClientExt) { }

    addReferralReasonGroupRecords(referralReasonGroup: IReferralReasonGroup): Observable<IReferralReasonGroup> {
        return this.http.post(this.controllerApi, referralReasonGroup).pipe(
            map(res => res as IReferralReasonGroup));
    }

    // getReferralReasonGroupAssociateByReasonId(id: number): Observable<IReferralReasonGroup[]> {
    //     const url = `${this.controllerApi}/getReferralReasonGroupByReasonId/${id}`;
    //     return this.http.get(url)
    //         .map(res => {
    //             return res as IReferralReasonGroup[] || [];
    //         });
    // }

    // //ReferralReason Group Associate
    // getReferralReasonGroupAssociateByGroupId(id: number): Observable<IReferralReasonGroup[]> {
    //     const url = `${this.controllerApi}/getReferralReasonGroupByGroupId/${id}`;
    //     return this.http.get(url)
    //         .map(res => {
    //             return res as IReferralReasonGroup[] || [];
    //         });
    // }

    // upsertReferralReasonGroupAssociate(items: IReferralReasonGroup[]): Observable<IReferralReasonGroup[]> {
    //     const url = `${this.controllerApi}/upsertReferralReasonGroupAssociate`;

    //     return this.http.post(`${url}`, items)
    //         .map(res => {
    //             return res as IReferralReasonGroup[] || [];
    //         })
    // }

    private handleErrorObservable(error: Response | any) {
        console.error(`ReferralReasonGroupService: ${error}`);
        return observableThrowError(error.message || error);
    }

}