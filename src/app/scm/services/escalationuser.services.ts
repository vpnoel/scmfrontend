
import {throwError as observableThrowError,  Observable, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IEscalationUser } from "../interfaces/IEscalationUser";


@Injectable()
export class EscalationUserService {
    private controllerApi = `${AppSettings.SITE_HOST}/api/EscalationUsers`;
    //private controllerApi = '/api/EscalationUser'

    constructor(private http: HttpClientExt) { }

    getEscalationUserRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const escalationUserDetail = res as any || {};
                return escalationUserDetail;
            }));
    }

    getEscalationUserRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const escalationUserDetails = res as any || {};
                return escalationUserDetails
            }));
    }


    private handleErrorObservable(error: Response | any) {
        console.error(`EscalationUserService: ${error}`);
        return observableThrowError(error.message || error);
    }

}