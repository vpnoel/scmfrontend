
import {throwError as observableThrowError,  Observable, Subject } from 'rxjs';

import {map} from 'rxjs/operators';
import { Injectable } from "@angular/core";
import { Http, Headers, Response, RequestOptions } from '@angular/http';

import { HttpClientExt } from "../../shared/services/httpclient.services";
import { AppSettings } from '../../shared/app.settings';
import { IReferralTransactionHistory } from "../interfaces/IReferralTransactionHistory";


@Injectable()
export class ReferralTransactionHistoryService {
    //private controllerApi = `${AppSettings.SITE_HOST}/api/ReferralTransactionHistory`;
    private controllerApi = '/api/ReferralTransactionHistory'

    constructor(private http: HttpClientExt) { }

    addReferralHistoryRecords(referralTransactionHistory: IReferralTransactionHistory): Observable<IReferralTransactionHistory> {
        return this.http.post(this.controllerApi, referralTransactionHistory).pipe(
            map(res => {
                return res as IReferralTransactionHistory
            }));
    }

    getReferralHistoryRecordById(id: number): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.get(url).pipe(
            map(res => {
                const referralTransactionHistoryDetail = res as any || {};
                return referralTransactionHistoryDetail;
            }));
    }

    getReferralHistoryRecords(): Observable<any> {
        return this.http.get(this.controllerApi).pipe(
            map(res => {
                const referralTransactionHistoryDetails = res as any || {};
                return referralTransactionHistoryDetails;
            }));
    }

    updateReferralHistoryRecords(id: number, data: any): Observable<any> {
        const url = `${this.controllerApi}/${id}`;

        return this.http.put(url, data).pipe(
            map(res =>
                res as any || {}
            ));
    }


    private handleErrorObservable(error: Response | any) {
        console.error(`ReferralTransactionHistoryService: ${error}`);
        return observableThrowError(error.message || error);
    }

}