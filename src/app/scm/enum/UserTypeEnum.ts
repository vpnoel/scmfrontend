export enum UserTypeEnum {
    Staff = "1",
    Teacher = "2",
    Driver = "3"
}