﻿import { IReferralTransaction } from "./IReferralTransaction";

export interface IReferralTransactionHistory {
    Id: number,
    ReferralTransactionId: number,
    HistoryAction: string,
    Date?: Date,
    ReferralTransaction: IReferralTransaction
}