export interface IReferralTransactionReasons {
    Id: number,
    ReferralReasonId: number,
    ReferralTransactionId: number,
    Priority?: number,
    CreatedBy: number,
    Created: Date,
    LastUpdatedBy?: number,
    LastUpdated?: Date,
}