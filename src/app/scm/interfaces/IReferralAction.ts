﻿export interface IReferralAction {
    ReferralActionId: number,
    Action: string,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date
}