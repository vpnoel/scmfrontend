import { IUserPermissionGroup } from "./IUserPermissionGroup";
import { IRoleFeature    } from "./IRoleFeature";

export interface IUserPermission {
    Id: number,
    UserPermissionGroupId: number,
    RoleFeaturesId: number,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated: Date,
    UpdatedBy: number,
    DtLastUpdated: Date,
    UserPermissionGroup: IUserPermissionGroup,
    RoleFeature: IRoleFeature
    
}