import { IReferralReason } from "./IReferralReason";
import { IReferralGroup } from "./IReferralGroup";

export interface IReferralReasonGroup {
    Id: number,
    ReferralReasonsId: number,
    ReferralGroupId: number,
    IsAssociated: boolean,
    Created: number,
    DtCreated: Date,
    ReferralReasons: IReferralReason,
    ReferralGroup: IReferralGroup
}

export interface IReferralReasonGroupMatrixData {
    ReferralReason: IReferralReason,
    ReferralreasonGroupAssociate: IReferralReasonGroup[]
}