﻿export interface INGXPieChartModel {
    name: string,
    value: number
}