export interface IDriver {
    DriverId: number,
    DriverExternalId: string,
    DriverName: string,
    EmployeeNumber: number,
    HireDate? : Date,
    HomeNumber: string,
    CellNumber: string,
    SmartTagId: number,
    DriverTypeId: number,
    ISDId: number,
    CreatedBy: string,
    DtCreated?: Date,
    LastUpdatedBy: string,
    DtLastUpdated?: Date,
    IsActive: boolean,
    ProviderRecordId: number,
    PINCode: string,
    ResetPin: any,
    RouteGroupId: number
}