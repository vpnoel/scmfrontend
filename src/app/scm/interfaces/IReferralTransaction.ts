﻿import { IReferralGroup } from "./IReferralGroup";
import { IReferralType } from "./IReferralType";
import { IReferralReason } from "./IReferralReason";
import { IStudent } from "./IStudent";
import { ICampus } from "./ICampus";
import { IReferralAction } from "./IReferralAction";
import { IISD } from "./IISD";
import { IISDStaff } from "./IISDStaff";
import { IBus } from "./IBus";
import { IParent } from "./IParent";
import { IReferralTransactionReasons } from "./IReferralTransactionReason";
import { IReferralTransactionGroups } from "./IReferralTransactionGroups";
import { ITeacher } from "./ITeacher";
import { IGuardian } from "./IGuardian";
import { IDriver } from "./IDriver";

export interface IReferralTransaction {
    ReferralId: number,
    StudentId: number,
    CampusId: number,
    TeacherId: number,
    DriverId: number,
    RouteId: number,
    ReferralTypeId: number,
    ReferralGroupId: number,
    Date?: Date,
    ReferralReasonsId: number,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    TimeOfDay: number,
    Explanation: string,
    ReferralStatus: number,
    IsResolved: boolean,
    IncidentLocation: string,
    GuardianId: number,
    PerceivedMotivation: string,
    AssociatedVideo: string,
    ReferralActionId: number,
    AdditionalDesc: string,
    AdminReport: string,
    DataPrivacy: string,
    ReferralCompletedByDiscProvider: boolean,
    EscalationUserId: number,
    BusId: number,
    DateOfIncident: Date,
    ParentId: number,
    IncidentGrade: string,
    IsIncidentReport: boolean,
    SuspensionStartDate?: Date,
    SuspensionEndDate?: Date,
    Restitution: string,
    SchoolFeedback: string,
    VideoButtonPressed: number,
    ReviewVideo: number,
    ReferralDateClosed: Date,
    ReferralDateEscalated: Date,
    EscalationNotes: string,
    IsDriver: boolean,
    Student: IStudent,
    Teacher: ITeacher,
    Campus: ICampus,
    Guardian: IGuardian,
    Driver: IDriver,
    Parent: IParent,
    ReferralGroup: IReferralGroup,
    ReferralType: IReferralType,
    ReferralReason: IReferralTransactionReasons,
    ReferralAction: IReferralAction,
    ISD: IISD
}

export interface CustomReferralTransaction {
    ReferralTransaction : IReferralTransaction,
    ReferralTransactionGroups: IReferralTransactionGroups[],
    ReferralTransactionReasons : IReferralTransactionReasons[]
}

export interface CustomDriverReferralTransaction{
    ReferralTransaction : IReferralTransaction,
    ReferralTransactionReasons : IReferralTransactionReasons[]
}