import { IReferralAction } from "./IReferralAction";
import { IReferralGroup } from "./IReferralGroup";

export interface IReferralActionGroup {
    Id: number,
    ReferralActionId: number,
    ReferralGroupId: number,
    IsAssociated: boolean,
    Created: number,
    DtCreated: Date,
    ReferralActions: IReferralAction,
    ReferralGroup: IReferralGroup
}

export interface IReferralActionGroupMatrixData {
    ReferralAction: IReferralAction,
    ReferralactionGroupAssociate: IReferralActionGroup[]
}