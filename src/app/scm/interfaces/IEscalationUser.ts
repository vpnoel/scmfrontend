﻿import { ICampus } from "./ICampus";
import { IISD } from "./IISD";

export interface IEscalationUser {
    EscalationUserId: number,
    CampusId: number,
    ISDId: number,
    EscalationUserName: string,
    Campus: ICampus,
    ISD: IISD
}