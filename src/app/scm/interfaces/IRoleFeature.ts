export interface IRoleFeature {
    Id: number,
    RoleNameKey: string,
    RoleName: string,
    IsActive: boolean
}