export interface IReferralTransactionGroups {
    Id: number,
    ReferralGroupId: number,
    ReferralTransactionId: number,
    Priority?: number,
    CreatedBy: number,
    DtCreated: Date,
    LastUpdatedBy?: number,
    DtLastUpdated?: Date,
}