﻿import { IISDStaff } from "./IISDStaff";
import { IISD } from "./IISD";

export interface IReferralGroup {
    ReferralGroupId: number,
    ReferralGroup1: string,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    IsAdmin: boolean,
    ISDId: number,
    ISD: IISD,
    ISDStaff: IISDStaff

}