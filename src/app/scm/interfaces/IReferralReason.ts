﻿export interface IReferralReason {
    ReferralReasonsId: number,
    ReferralReasons1: string,
    IsActive: boolean,
    CreatedBy: number,
    DtCreated?: Date,
    LastUpdatedBy: number,
    DtLastUpdated?: Date,
    ReferralTypeId: number
}