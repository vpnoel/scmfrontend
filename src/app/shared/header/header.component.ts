import { Component, HostListener, Inject, OnInit, Renderer, ElementRef } from "@angular/core";
import { DOCUMENT } from '@angular/platform-browser';
import { WINDOW } from "../services/windows.service";
import * as $ from 'jquery'

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public darkHeader: boolean = false;  
  public menuItems: any[];
  
  // Inject Document object
  constructor(
    @Inject(DOCUMENT) private document: Document,
    @Inject(WINDOW) private window,
  ) { 
    window.addEventListener('scroll', this.onWindowScroll, true); //third parameter
  }

    ngOnInit() {
        $.getScript('./assets/js/script.js', function( data, textStatus, jqxhr ) {
          console.log( data ); // Data returned
          console.log( textStatus ); // Success
          console.log( jqxhr.status ); // 200
          console.log( "Load was performed." );
        });
        $.getScript('./assets/js/tilt.jquery.js', function( data, textStatus, jqxhr ) {
          console.log( data ); // Data returned
          console.log( textStatus ); // Success
          console.log( jqxhr.status ); // 200
          console.log( "Load was performed." );
        });
        this.onScrollDetectSection();
    }

    onScrollDetectSection(): void {
        var sections = $('section')
            , nav = $('nav')
            , navHeight = nav.outerHeight();

        $(window).on('scroll', function () {
            var currentPosition = $(this).scrollTop();

            sections.each(function () {
                var top = $(this).offset().top - navHeight,
                    bottom = top + $(this).outerHeight();

                if (currentPosition >= top && currentPosition <= bottom) {
                    nav.find('a').removeClass('active');
                    sections.removeClass('active');

                    $(this).addClass('active');
                    nav.find('a[href="#' + $(this).attr('id') + '"]').addClass('active');
                }
            });
        });
    }

  

    // @HostListener Decorator
    @HostListener("window:scroll", []) 
    onWindowScroll() {
        let number = this.window.pageYOffset || this.document.documentElement.scrollTop || this.document.body.scrollTop || 0;
        if (number >= 60) { 
          this.darkHeader = true;
        } else {
          this.darkHeader = false;
        }
    }

}
