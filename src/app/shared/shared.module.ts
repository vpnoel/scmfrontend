﻿// Core Modules
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgxPageScrollModule } from 'ngx-page-scroll';
import { CommonModule } from '@angular/common';

// Components
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';

// Shared Services
import { RedirectComponent } from './components/redirect/redirect.component';
import { HttpClientExt } from './services/httpclient.services';
import { AuthService } from './services/auth.service';
import { DataService } from './services/data.service';
import { LandingFixService } from './services/landing-fix.service';
import { ModalService } from './services/modal.services'
import { WINDOW_PROVIDERS } from "./services/windows.service"


@NgModule({
    declarations: [
        RedirectComponent,
        HeaderComponent,
        FooterComponent
    ],
    imports: [
        CommonModule,
        HttpClientModule,
        RouterModule,
        NgxPageScrollModule,
    ],
    exports: [
        HeaderComponent,
        FooterComponent
    ],
    providers: [
        HttpClientExt,
        AuthService,
        DataService,
        WINDOW_PROVIDERS,
        LandingFixService,
    ]
})

export class SharedModule {
    constructor() { }
} 